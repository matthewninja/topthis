var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Top This!'});
});

router.get('/top', function (req, res, next) {
    res.render('top', {title: 'Top This!'});
});

module.exports = router;